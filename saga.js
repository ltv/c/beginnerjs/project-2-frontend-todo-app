import { call, put, delay, takeLatest, takeEvery } from 'redux-saga/effects';
import * as taskTypes from './../contants/task';
import { STATUSES } from './../contants/index';
import { getList, addTask } from '../apis/task';
import * as taskActions from './../actions/task';
import * as uiActions from './../actions/ui';
import * as modalActions from './../actions/modal';
import { STATUS_CODE } from '../contants/index';

/**
 * B1 : Thuc thi action lay danh sach Fetch Task
 * B2 : Goi API
 * B3 : Kiem tra Status code 
 * B4 : Thuc thi cong viec tiep theo
 */

function* fetchListTaskSaga({payload}){
  let {params} = payload;
  yield put(uiActions.showLoading());
  try{
    const res = yield call(getList,params);
    let { status, data } = res;
    if (status === STATUS_CODE.SUCCESS) {
      yield put(taskActions.fetchListTaskSuccess(data));
    } else {
      yield put(taskActions.fetchListTaskFailed(data));
    }
  }catch(e){
    yield put(taskActions.fetchListTaskFailed(e));
  }
  yield delay(500);
  yield put(uiActions.hideLoading());
}

function* filterTaskSaga({ payload }) {
  yield delay(600);
  let {keyword} = payload;
  yield put(taskActions.fetchListTask({
    q : keyword
  }));

}

function* addTaskSaga({ payload }) {
  let { title, description } = payload;
  yield put(uiActions.showLoading());
  try {
    let res = yield call(addTask, {
      title,
      description,
      status: STATUSES[0].value
    });
    let { data, status } = res;
    if (status === STATUS_CODE.CREATED) {
      yield put(taskActions.addTaskSuccess(data));
      yield put(modalActions.hideModal());
    } else {
      yield put(taskActions.addTaskFailed(data));
    }
  } catch (error) {
    yield put(taskActions.addTaskFailed(error));
  }
  yield delay(1000);
  yield put(uiActions.hideLoading());
}

function* rootSaga() {
  yield takeLatest(taskTypes.FETCH_TASK, fetchListTaskSaga);
  yield takeLatest(taskTypes.FILTER_TASK, filterTaskSaga);
  yield takeEvery(taskTypes.ADD_TASK, addTaskSaga);
}

export default rootSaga;